import sys

try:
    from setuptools import setup
except ImportError:
    print("Distribute is required for install:")
    print("\thttp://python-distribute.org/distribute_setup.py")
    sys.exit(1)

if sys.version_info <= (2, 6):
    print("Python 2.6 or greater is required.")
    sys.exit(1)

# Python 3 conversion
extra = {}
if sys.version_info >= (3,):
    extra['use_2to3'] = True

description = "A video ingestion daemon"

requires = [
    'docopt'
]

setup(
    name='Videopede',
    version='0.1',
    description=description,
    long_description='TODO',
    author='Kevin Landreth',
    author_email='crackerjackmack@gmail.com',
    packages=[
        'videopede'
    ],
    license='MIT',
    zip_safe=True,
    url='https://github.com/CrackerJackMack/videopede',
    entry_points={
        'console_scripts': [
            'videopede = videopede:invoke',
        ],
    },
    test_suite='nose.collector',
    install_requires=requires,
    classifiers=[
        'Environment :: Console',
        'Environment :: Web Environment',
        'Intended Audience :: System Administrators',
        'Intended Audience :: End Users/Desktop',
        'License :: OSI Approved :: MIT License',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 2.6',
        'Programming Language :: Python :: Implementation :: CPython',
    ],
    **extra
)
