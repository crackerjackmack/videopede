"""
Usage: videopede <configfile>

"""
from docopt import docopt
from config import parse_config, InvalidConfig
from const import VERSION
import sys


def invoke():
    arg = docopt(__doc__, version=VERSION)

    with open(arg['<configfile>']) as cfg:
        try:
            config = parse_config(cfg)
        except InvalidConfig as e:
            print(e)
            sys.exit(1)

    print(config.get('main', 'encoder'))

if __name__ == "__main__":
    invoke()
