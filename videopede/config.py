from ConfigParser import ConfigParser


class InvalidConfig(RuntimeError):
    pass


def parse_config(config_fp):
    config = ConfigParser()
    config.readfp(config_fp)

    if not config.has_section('main'):
        raise InvalidConfig('missing section "main"')

    encoder = config.get('main', 'encoder')
    if encoder and not config.has_section('encoder:%s' % encoder):
        raise InvalidConfig('Encoder "%s" has no config section' % encoder)

    uploader = config.get('main', 'uploader')
    if uploader and not config.has_section('uploader:%s' % uploader):
        raise InvalidConfig('Uploader "%s" has no config section' % encoder)

    return config
