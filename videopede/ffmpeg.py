"""
    Ubuntu users:
    If you have problems with "unrecognized encoder" or anything
    like that see if this "bug" report fixes it.
    https://bugs.launchpad.net/ubuntu/+source/libav/+bug/1173343
"""

import os
from subprocess import Popen, PIPE


def encode_file(config, infile, outfile):
    input_size = os.path.getsize(infile)
    read_size = 4194304
    bytes_read = 0
    progress = 0.00
    process = launch_encoder(config, outfile)

    for chunk in read_chunk(infile, read_size):
        # Can't use .communicate() because it doesn't handle
        # iterators that I could tell.  Will stick with .write() for now
        # http://goo.gl/d9z3CT
        process.stdin.write(chunk)
        bytes_read = bytes_read + read_size
        progress = float(bytes_read) / float(input_size)
        yield progress

    process.wait()


def read_chunk(infile, size):
    with open(infile, 'r') as r:
        while True:
            yield r.read(size)


def launch_encoder(config, outfile):
    cmd = [
        'ffmpeg',
        '-i', '-',
        '-c:v', 'libx264',
        '-preset', config.get('preset', 'slow'),
        '-tune', config.get('tune', 'animation'),
        '-crf', config.get('crf', 18),
        '-c:a', 'copy',
        '-pix_fmt', config.get('pixfmt', 'yuv420p'),
        outfile,
    ]

    return Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
