try:
    import unittest2 as unittest
except ImportError:
    import unittest  # NOQA
from tempfile import mkstemp
import os
import videopede


class ConfigTest(unittest.TestCase):
    def setUp(self):
        self.file_id, self.config_file = mkstemp(text=True)
        self.config_fd = os.fdopen(self.file_id, 'r+w')

    def tearDown(self):
        self.config_fd.close()
        os.unlink(self.config_file)

    def valid_section_test(self):
        self.config_fd.write("""[main]
        \nencoder = test
        \nuploader = test

        \n[encoder:test]

        \n[uploader:test]
        """)
        self.config_fd.seek(0)

        c = videopede.parse_config(self.config_fd)
        self.assertIsNotNone(c)

    def invalid_encoder_test(self):
        self.config_fd.write("""[main]
        \nencoder = test4
        \nuploader = test

        \n[encoder:test]

        \n[uploader:test]
        """)
        self.config_fd.seek(0)

        self.assertRaises(videopede.InvalidConfig,
                          videopede.parse_config,
                          self.config_fd)

    def invalid_uploader_test(self):
        self.config_fd.write("""[main]
        \nencoder = test
        \nuploader = tes5t

        \n[encoder:test]

        \n[uploader:test]
        """)
        self.config_fd.seek(0)

        self.assertRaises(videopede.InvalidConfig,
                          videopede.parse_config,
                          self.config_fd)

    def empty_uploader_test(self):
        self.config_fd.write("""[main]
        \nencoder = test
        \nuploader =

        \n[encoder:test]

        \n[uploader:test]
        """)
        self.config_fd.seek(0)

        c = videopede.parse_config(self.config_fd)
        self.assertIsNotNone(c)
